import 'package:flutter/material.dart';

//App Gradient
const gradient = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [
    darkBlue,
    green,
  ],
);


const Color darkBlue = Color(0xff5761b2);
const Color darkBlue2 = Color(0xff444b8c);
const Color green = Color(0xff1fc5a8);
const Color green2 = Color(0xff50e8c9);


const Color white = Colors.white;
const Color black = Colors.black;
const Color borderColor = Color(0xffbbbbbb);
const Color scaffoldBackground = Color(0xffe7e7e7);
const Color grey = Color(0xffe5e5e5);