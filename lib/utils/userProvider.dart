import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:contact_manager/model/userModel.dart';

const apiUrl = "https://randomuser.me/api";

class UserProvider with ChangeNotifier{
  List<UserModel> _userData = [];

  List<UserModel> get userData {
    return [..._userData];
  }

  Future<void> getUsers(String userId) async {
    String url = '${apiUrl}get/user?userId=$userId';

    List<UserModel> userData = [];

    log(url);

    try {
      final response = await http.get(
        Uri.parse(url),
        headers: {"Content-Type": "application/json"},
      );
      var userResponse =
      json.decode(response.body) as Map<String, dynamic>;

      log(userResponse.toString());

      if (userResponse['status'] == 0) {
        throw HttpException(userResponse['message']);
      }

      if (userResponse['status'] == 1) {
        var data = userResponse["data"] as List<dynamic>;
        for (var ele in data) {
          // log('we are loading the list');
          userData.add(
            UserModel.fromJson(ele)
          );
        }
        log(userData.toString());
        _userData = userData;
      } else {
        throw HttpException(userResponse['message']);
      }
    } catch (e) {
      rethrow;
    }
  }
}