import 'package:flutter/material.dart';
import 'package:contact_manager/utils/colors.dart';
import 'package:sizer/sizer.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {


  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          stops: [0.1, 0.5],
          colors: [
            green2,
            white,
          ],
        )
      ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
      body: SafeArea(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 3.w, vertical: 2.h),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  // Row(
                  //   children: [
                  //     GestureDetector(
                  //       onTap: (){
                  //         Navigator.push(
                  //           context,
                  //           MaterialPageRoute(builder: (context) => ProfileScreen()),
                  //         );
                  //       },
                  //       child: CircleAvatar(
                  //         radius: 23,
                  //         backgroundColor: white.withOpacity(0.8),
                  //         child: const Icon(
                  //           Icons.person_outline,
                  //           color: darkBlue2,
                  //           size: 27,
                  //         ),
                  //       ),
                  //     ),
                  //     SizedBox(
                  //       width: 3.w
                  //     ),
                  //     Text(
                  //       username,
                  //       style: TextStyle(fontSize: 12.sp, color: black),
                  //     )
                  //   ],
                  // ),
                ],
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(left: 5.w, top: 5.h),
                child: Text(
                  "Hello, Kripa!",
                  style: TextStyle(
                      color: black,
                      fontSize: 18.sp,
                      fontWeight: FontWeight.bold
                  ),
                )
            ),
            Container(
              alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(left: 5.w, top: 1.h, bottom: 5.h),
                child: Text(
                  "Let's explore what's happening nearby",
                  style: TextStyle(
                      color: black,
                      fontSize: 16.sp,
                      fontWeight: FontWeight.w500
                  ),
                )
            ),

            // Expanded(
            //   child: ListView.builder(
            //     physics: BouncingScrollPhysics(),
            //       itemCount: meetupList.length,
            //       itemBuilder: (context, index) => MeetupCard(
            //         meetupModel: meetupList[index],
            //       )
            //   ),
            // )
          ],
        ),
      ),
    ));
  }
}
