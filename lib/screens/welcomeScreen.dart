import 'package:contact_manager/screens/homeScreen.dart';
import 'package:flutter/material.dart';
import 'package:contact_manager/utils/colors.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  double opacity = 0.0;

  @override
  void initState() {
    super.initState();
    changeOpacity();
  }

  changeOpacity() {
    Future.delayed(Duration(milliseconds: 1300), () {
      setState(() {
        opacity = 1.0;
        changeOpacity();
      });
      Get.to(() => const HomeScreen());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: white,
        body: SingleChildScrollView(
          child: Column(children: [
            Container(
                padding: EdgeInsets.only(top: 50.h, left: 10.w, right: 20.w),
                alignment: Alignment.centerLeft,
                child: Text(
                  'Welcome to Contact Manager.',
                  style: TextStyle(
                      color: black,
                      fontSize: 28.sp,
                      fontWeight: FontWeight.bold),
                )),
            AnimatedOpacity(
              opacity: opacity,
              duration: const Duration(milliseconds: 1300),
              curve: Curves.decelerate,
              child: Container(
                  padding: EdgeInsets.only(top: 2.h, left: 10.w, right: 10.w),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Connect with like-minded people.',
                    style: TextStyle(color: darkBlue, fontSize: 16.sp),
                  )),
            ),

          ]),
        ));
  }
}

