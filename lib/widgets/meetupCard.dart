import 'package:flutter/material.dart';
import 'package:contact_manager/model/userModel.dart';
import 'package:contact_manager/screens/eventDetailsScreen.dart';
import 'package:contact_manager/utils/colors.dart';
import 'package:sizer/sizer.dart';

class UserCard extends StatefulWidget {
  UserModel userModel;

  UserCard({Key? key, required this.userModel}) : super(key: key);

  @override
  State<UserCard> createState() => _UserCardState();
}

class _UserCardState extends State<UserCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  EventDetailsScreen(userModel: widget.userModel)),
        );
      },
      child: Container(
        padding: EdgeInsets.only(left: 5.w),
        margin: EdgeInsets.symmetric(vertical: 2.h, horizontal: 3.w),
        height: 17.h,
        decoration: BoxDecoration(
          color: grey.withOpacity(0.4),
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Container(
                    width: 45.w,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${widget.userModel.name}",
                          style: TextStyle(
                              fontSize: 14.sp,
                              fontWeight: FontWeight.bold,
                              color: darkBlue2),
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.calendar_month_rounded,
                              size: 12.sp,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 2.w),
                              width: 45.w,
                              child: Text(
                                widget.userModel.dob!,
                                style: TextStyle(
                                    fontSize: 12.sp,
                                    // fontWeight: FontWeight.w500,
                                    color: black),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Icon(
                              Icons.location_on_rounded,
                              size: 12.sp,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 2.w),
                              width: 45.w,
                              child: Text(
                                widget.userModel.street!,
                                style: TextStyle(
                                    fontSize: 12.sp,
                                    // fontWeight: FontWeight.w500,
                                    color: black),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                ClipRRect(
                  borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomRight: Radius.circular(20)),
                  child: Image.network(
                    widget.userModel.picUrl!,
                    fit: BoxFit.cover,
                    height: 17.h,
                    width: 35.w,
                    loadingBuilder: (BuildContext context, Widget child,
                        ImageChunkEvent? loadingProgress) {
                      if (loadingProgress == null) return child;
                      return Center(
                        child: CircularProgressIndicator(
                          color: darkBlue2,
                          value: loadingProgress.expectedTotalBytes != null
                              ? loadingProgress.cumulativeBytesLoaded /
                              loadingProgress.expectedTotalBytes!
                              : null,
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
