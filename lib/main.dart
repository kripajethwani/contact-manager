import 'package:contact_manager/utils/userProvider.dart';
import 'package:flutter/material.dart';
import 'package:contact_manager/screens/welcomeScreen.dart';
import 'package:contact_manager/utils/colors.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:get/get.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => UserProvider()),
      ],
      child: Sizer(builder: (context, orientation, deviceType) {
        return GetMaterialApp(
          title: 'Contact Manager',
          theme: ThemeData(
            fontFamily: 'Lato',
            primaryColor: black,
            // scaffoldBackgroundColor: scaffoldBackground,
            // accentColor: Color(0xFFccf869),
          ),
          home: const WelcomeScreen(),
        );
      }),
    );
  }
}
