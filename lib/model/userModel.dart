class UserModel {
  String? gender;
  String? name;
  String? street;
  String? city;
  String? state;
  String? country;
  String? email;
  String? phone;
  String? dob;
  String? picUrl;


  UserModel({
    required this.name,
    this.gender,
    this.street,
    this.city,
    this.state,
    this.country,
    this.email,
    this.phone,
    this.dob,
    this.picUrl,
  });

  UserModel.fromJson(Map<String, dynamic> json) {
    name = json['name'] ?? "";
    street = json['location']['street']['name'] ?? "";
    city = json['location']['city'] ?? "";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['location']['street']['name'] = this.street;
    data['location']['city'] = this.city;
    return data;
  }
}